const colors = require('tailwindcss/colors')
module.exports = {
    purge: [],
    darkMode: false,
    theme: {

        fontFamily: {
            'sans': ['"noto_sansregular"'],
            'header': ['"noto_sansbold"'],
        },

        container: {
            center: true,
        },
        backgrounds: {
            transparent: 'transparent',
            white: '#FFFFFF',
            black: '#000000',
            primary: '#a8804c',
            secondary: '#ded7cd',
            lighterbg: '#F9F7F5'
        },

        colors: {
            transparent: 'transparent',
            white: '#FFFFFF',
            black: '#000000',
            primary: '#a8804c',
            secondary: '#ded7cd',
            lighterbg: '#F9F7F5'
        },
        extend: {
            letterSpacing: {
                'h1': '0.699rem',
                'h2': '0.439rem',
                'h3': '0.13rem',
                'para': '0.03rem',
                'btn':'0.146rem'
            },
            maxWidth: {
                '1100': '68.75rem',
                '1200': '75rem',
                '1600': '100rem',
                '560': '35rem',
                '590': '36.875rem',
                '600': '37.5rem',
                '360': '22.5rem',
                '450': '28.125rem',
                '188':'11.75rem',
                '270':'16.875rem',
                '138':'8.625rem',
                '384':'24rem',
                '165':'10.3125rem'
            },
            maxHeight: {},
            minHeight: {
                '836':'52.25rem',
                '750':'46.875rem'
            },
            lineHeight: {
                'mainh1': '7.3125rem',
                'mainh2': '4.625rem',
                'mainh3': '1.375rem',
                'para': '1.625rem',
            },
            padding: {
                '60': '3.75rem',
                '35':'2.1875rem',
                '13':'3.25rem',
                '18':'1.125rem',
                '100':'6.25rem',
                '131':'8.1875rem',
                '150':'9.375rem',
                '160':'10rem',
                '46':'2.875rem',
                '38':'2.375rem',
                '50':'3.125rem',
                '230':'14.375rem',
                '200':'12.5rem',
                '72':'4.5rem',
                '33':'2.0625rem',
                '75':'4.6875rem',
                '134': '8.375rem'
            },
            margin: {
                '-114':'-7.125rem',
                '-70':'-4.375rem',
                '34':'2.125rem'
            },
            spacing: {
                '47':'2.9375rem',
                '133':'8.3125rem'
            },
            width: {
                '384':'24rem',
            },
            height: {
                '134': '8.375rem'
            },
            fontSize: {
                'h1': '5.375rem',
                'h2': '3.375rem',
                'h3': '1rem',
                'h4': '1.625rem',
                'h5': '1.125rem',
                'h6': '1rem',
                'p': '1rem', 
                'smh2':'1.875rem'
            },
            inset: {
                '60r': '3.75rem',
                '70b': '4.375rem',
                '108':'6.75rem'
            },
            zIndex: {
                '999': 999,
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}